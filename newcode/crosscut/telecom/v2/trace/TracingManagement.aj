package telecom.v2.trace;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.aspectj.lang.JoinPoint;
import telecom.v2.trace.Tracer;
import telecom.v2.connect.*;
import telecom.v2.common.Pointcuts;
import telecom.v2.billing.BillManagement.Type;

/**
 * Aspect définissant les advices permettant de tracer l'application
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public privileged aspect TracingManagement {
	
	/**
	 * Redéfinition de la méthode toString() des Call
	 * @return chaîne représentant un Call
	 */
	public String telecom.v2.connect.Call.toString() {
		String sRet = "<";
		
		sRet += this.getCaller().getName();
		sRet += "|";
		sRet += customersToString(this.pending.keySet());
		sRet += "|";
		sRet += customersToString(this.complete.keySet());
		sRet += "|";
		sRet += customersToString(this.dropped.keySet());
		
		return sRet + ">";
	}
	
	/**
	 * Converti un Set<ICustomer> en une liste de nom séparé par des espaces
	 * @param set Set<ICustomer>
	 * @return liste de nom séparé par des espaces
	 */
	public static final String customersToString(Set<ICustomer> set) {
		String sRet = "";
		boolean bFirst = true;
		for(ICustomer c  : set) {
			if(bFirst) {
    			sRet += c.getName();
    			bFirst = false;
    		} else {
    			sRet += " " + c.getName();
    		}
    	}
		return sRet;
	}
	
	/**
	 * Advice qui trace avant les appels de méthodes de ICall et ICustomer
	 * @param x
	 */
	before(Object x) : (Pointcuts.ICallFinal() || Pointcuts.ICustomerFinal()) && target(x) {
		JoinPoint jp = thisJoinPoint;
		String methName = jp.getSignature().getName();
		SimulationMessages sm = SimulationMessages.get(x.getClass(), methName);
		Tracer.trace(sm.format(jp));
		Tracer.indent();
	}
	
	/**
	 * Advice qui trace après les appels de méthodes de ICall et ICustomer
	 * @param x
	 */
	after(Object x) : (Pointcuts.ICallFinal() || Pointcuts.ICustomerFinal()) && target(x) {
		JoinPoint jp = thisJoinPoint;
		SimulationMessages sm = SimulationMessages.get(x.getClass(), "final");
		Tracer.unindent();
		Tracer.trace(sm.format(jp));
		if(Tracer.getLevel() == 0) {
			Tracer.jumpLine();
		}
	}
	
	/**
	 * Advice qui trace les changements de State de Connection
	 * @param conn
	 */
	void around(telecom.v2.connect.Connection conn) : Pointcuts.ConnectionChangeState() && target(conn) {
		telecom.v2.connect.Connection.State oldState = conn.state;
		proceed(conn);
		telecom.v2.connect.Connection.State newState = conn.state;
		String connStr = conn.toString();
		connStr = connStr.substring(connStr.lastIndexOf('.')+1);
		Tracer.trace(connStr + "(" + oldState + " -> " + newState + ")");
	}

	/**
	 * Advice qui trace la fin d'une connection
	 * @param conn
	 */
	after(telecom.v2.connect.Connection conn) : Pointcuts.ConnectionDrop() && target(conn) {
		Tracer.trace("temps de connexion : "+conn.getDuration()+" s");
		String priceStr = "montant de la connexion ";
		if(conn.type == Type.LOCAL) {
			priceStr += "locale";
		} else if(conn.type == Type.NATIONAL) {
			priceStr += "longue distance";
		}
		Tracer.trace(priceStr + " : " + conn.getPrice());
	}
	
	/**
	 * Redéfinition du toString() de ICustomer pour un affichage dans le tracing
	 * @return Une chaîne représentant le nom et le code postal du ICustomer
	 */
	public String telecom.v2.connect.ICustomer.toString() {
		return this.getName() + "[" + this.getAreaCode() + "]";
	}
	
	/**
	 * Liste des Customers 
	 */
	private List<Customer> customersList;
	
	/**
	 * Initialisation de la liste de Customer au début d'un runTest*()
	 */
	before() : call(* telecom.v2.simulate.Simulation.runTest*(..)) {
		this.customersList = new ArrayList<Customer>();
	}
	
	/**
	 * Ajout d'un Customer dans la liste des customers lors d'une action d'un Customer
	 * @param customer
	 */
	after(Customer customer) : Pointcuts.ICustomerFinal() && within(telecom.v2.simulate.Simulation) && target(customer) {
		if(!this.customersList.contains(customer)) {
			this.customersList.add(customer);
		}
	}
	
	/**
	 * Résumé à la fin d'un Test
	 */
	after() : call(* telecom.v2.simulate.Simulation.runTest*(..)) {
		for(Customer cust : this.customersList) {
			String sTrace = cust.toString();
			if(cust.call != null  && !cust.call.noCalleePending()) {
				sTrace += " est en attente de ";
				for(ICustomer pendingCust : ((Call)cust.call).pending.keySet()) {
					sTrace += pendingCust.getName();
				}
				sTrace += " et son montant sera supérieur à " + cust.getCharge();
			} else {
				sTrace += " a été connecté " + cust.getTotalConnectedTime() + " s ";
				sTrace += "pour un montant de " + cust.getCharge();
			}
			Tracer.trace(sTrace);
		}
		Tracer.trace("------------------------------------------");
	}
	
}
