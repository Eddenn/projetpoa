package telecom.v2.trace;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe statique permettant le tracage de l'application à l'aide d'un Logger
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public class Tracer{

	private static final Logger LOGGER = Logger.getLogger(Tracer.class.getName());
	
    static {
    	SimulationFormatter formatter = new SimulationFormatter();
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(formatter);
        LOGGER.addHandler(consoleHandler);
        LOGGER.setUseParentHandlers(false);
    }

    private static Indenter indenter = new Indenter();
    
    public static void indent() {
    	indenter.indent();
    }
    
    public static void unindent() {
    	indenter.unindent();
    }
    
    public static int getLevel() {
    	return indenter.getLevel();
    }
    
    public static void trace(String msg) {
        LOGGER.log(Level.INFO, indenter.getSpacer()+msg);
    }
    
    public static void jumpLine() {
    	LOGGER.log(Level.INFO, "");
    }
}
