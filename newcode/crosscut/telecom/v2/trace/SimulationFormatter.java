package telecom.v2.trace;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Formatter personnalisé retirant le préfixe et le suffixe des messages
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public class SimulationFormatter extends Formatter {

	@Override
	public String format(LogRecord record) {
		return record.getMessage() + "\r\n";
	}

}
