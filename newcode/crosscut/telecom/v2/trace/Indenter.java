package telecom.v2.trace;

/**
 * Classe utilitaire permettant la gestion de l'indentation
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public class Indenter {

	private static String SPACER = "|  ";
	private int level;
	
	public Indenter() {
		this.level = 0;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void indent() {
		level++;
	}
	
	public void unindent() {
		level--;
	}
	
	public String getSpacer() {
		StringBuffer sRet = new StringBuffer(SPACER.length() * level);
		for (int i = 0; i < level; i++){
			sRet.append(SPACER);
		}
		return sRet.toString();
	}
}
