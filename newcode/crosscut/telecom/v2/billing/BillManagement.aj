package telecom.v2.billing;

import java.util.Map;
import java.util.Set;

import telecom.v2.common.Pointcuts;
import telecom.v2.connect.Call;
import telecom.v2.connect.Customer;
import telecom.v2.connect.ICall;
import telecom.v2.connect.ICustomer;
import telecom.v2.time.*;
import telecom.v2.util.Contract;

/**
 * Classe permettant de calculer le cout des appels de chaque personne
 * présente dans la communication
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public privileged aspect BillManagement {
    
    // On ajoute un attribut price de type int dans la classe Call
	private int telecom.v2.connect.ICall.price;
	
	/**
	* Méthode qui permet de récupérer le montant total que doit payer le client.
	* @return price
	*/
	private int telecom.v2.connect.ICall.getTotalPrice(){
		return this.price;
	}
	/**
	* Méthode qui permet de définir le prix
	* @param price
	*/
	private int telecom.v2.connect.ICall.setPrice(int price){
		return this.price = price;
	} 
	
	// On ajoute un attribut charge de type int dans la classe Customer
	private int telecom.v2.connect.ICustomer.charge;
	
	/**
	* Méthode qui permet de récupérer le montant dont le client doit s'acquitter.
	* @return charge 
	*/
	private int telecom.v2.connect.ICustomer.getCharge(){
		return this.charge;
	}
	
	/**
	* Méthode qui permet de définir le montant
	* @param charge
	*/

	private void telecom.v2.connect.ICustomer.setCharge(int c){
		this.charge = c;
	}
	
	// On ajoute un attribut type de type Type dans la classe Connection
	private Type telecom.v2.connect.Connection.type;
	
	/**
	* Méthode qui permet de récupérer le tarif à l'unité de la connexion.
	* @return type.rate 
	*/
    private int telecom.v2.connect.Connection.getRate(){
		return this.type.rate;
	}
    /**
	* Méthode qui permet de définir si l'appel est local ou national
	* @param charge
	*/
	private void telecom.v2.connect.Connection.setType(Type type){
		this.type = type;
	}

	/**
	* Méthode qui permet de récupérer le prix de la connection
	* @return un entier qui est calculé de la maniere suivante : la durée x le tarif
	*/
	public int telecom.v2.connect.Connection.getPrice() {
        Contract.checkCondition(this.state == State.DROPPED);

        return this.getDuration() * this.getRate();
    }
	
	/**
	 * Advice qui trace le montant que le client devra payer
	 * @param c
	 * @param con
	 */
	after(ICall c, telecom.v2.connect.Connection con) : Pointcuts.ConnectionDrop() && this(c) && target(con) {
		if(con.getState() != telecom.v2.connect.Connection.State.PENDING) {
			c.setPrice( c.getTotalPrice() + con.getPrice() ); 
		}
	}
	
	/**
	 * Advice qui trace le montant que doit s'acquitter le client
	 * @param c
	 */
	after(telecom.v2.connect.Connection c) returning : target(c) && Pointcuts.ConnectionDrop() {
		c.getCaller().setCharge( c.getCaller().getCharge() + c.getPrice());
	}
	
	/**
	 * Advice qui trace le type de l'appel, LOCAL ou NATIONAL
	 * @param o
	 */
    after(telecom.v2.connect.Connection o) : Pointcuts.ConnectionConstructorExec() && this(o){
    	if (o.caller.getAreaCode() == o.callee.getAreaCode()) {
    		o.type = Type.LOCAL;
        } else {
        	o.type = Type.NATIONAL;
        }
    }
	

	
	//Type of call
	public enum Type {
        LOCAL(3),
        NATIONAL(10);
        private int rate;
        Type(int r) {
            rate = r;
        }
        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }
}
