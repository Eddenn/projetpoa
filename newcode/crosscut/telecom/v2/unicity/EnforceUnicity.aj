package telecom.v2.unicity;

import telecom.v2.common.Pointcuts;
import telecom.v2.util.NotUniqueException;

/**
 * Aspect forçant l'unicité des champs annotés par @UniqueId
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
public aspect EnforceUnicity {
	
	declare error : Pointcuts.UniqueIdBadUsage() : "Bad usage of @UniqueId";
	
	/**
	 * Vérifie et gère l'unicité des champs annotés par @UniqueId
	 * @param id
	 */
	before(String id) : Pointcuts.UniqueIdGoodUsage() && args(id) {
		if(Unicity.getInstance().contains(id)) {
			throw new NotUniqueException();
		} else {
			Unicity.getInstance().add(id);
		}
	}
	
	/**
	 * Lance une exception dans le cas d'une mauvais utilisation de l'annotation @UniqueId
	 */
	before() : Pointcuts.UniqueIdBadUsage() {
		throw new AssertionError("Bad usage of @UniqueId");
	}
}
