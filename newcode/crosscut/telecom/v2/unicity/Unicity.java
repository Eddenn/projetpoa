package telecom.v2.unicity;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton stockant les id des champs annotés par @UniqueId
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 * 
 */
public class Unicity {

    private static Unicity INSTANCE = new Unicity();
     
    public static Unicity getInstance() {   
    	return INSTANCE;
    }
	
	private List<String> uniqueIds;
	
	private Unicity() {
		uniqueIds = new ArrayList<String>();
	}

	public boolean contains(String id) {
		for(String s : uniqueIds) {
			if(s.equals(id)) return true;
		}
		return false;
	}
	
	public void add(String id) {
		uniqueIds.add(id);
	}
	
	public void remove(String id) {
		int index = -1;
		for(int i=0; i<uniqueIds.size() ; i++) {
			if(uniqueIds.get(i).equals(id)) index = i;
		}
		if(index != -1) {
			uniqueIds.remove(index);
		}
	}
}
