package telecom.v2.unicity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation indiquant que le champs doit être valué par un id unique
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 *
 */
@Target(ElementType.FIELD) 
@Retention(RetentionPolicy.CLASS)
public @interface UniqueId {

}
