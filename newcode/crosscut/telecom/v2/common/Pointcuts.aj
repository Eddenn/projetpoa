package telecom.v2.common;

import telecom.v2.unicity.UniqueId;
import telecom.v2.connect.*;

/**
 * Classe stockant toutes les coupes des aspects :
 *  - EnforceUnicity
 *  - BillingManagement
 *  - TimeManagement
 *  - TracingManagement
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 * 
 */
public aspect Pointcuts {

	public pointcut UniqueIdBadUsage() : set(@UniqueId * *.*) && !UniqueIdGoodUsage();
	public pointcut UniqueIdGoodUsage() : set(@UniqueId final String *.*);

	public pointcut ConnectionConstructorExec() : execution(telecom.v2.connect.Connection.new(..));
	public pointcut ConnectionComplete() : call(* telecom.v2.connect.Connection.complete());
	public pointcut ConnectionDrop() : call (* telecom.v2.connect.Connection.drop());
	public pointcut CustomerHangUp() : call(* telecom.v2.connect.ICustomer.hangUp());
	public pointcut CallHangUp() : call(* telecom.v2.connect.ICall.hangUp(..));
	public pointcut CallConstructorExec() : execution(telecom.v2.connect.Call.new(..));
	
	public pointcut ICallInvite() : call(void telecom.v2.connect.ICall.invite(..));
	public pointcut ICallPickUp() : call(void telecom.v2.connect.ICall.pickUp(..));
	public pointcut ICallHangUp() : call(void telecom.v2.connect.ICall.hangUp(..));
	public pointcut ICallFinal() : ICallInvite() || ICallPickUp() || ICallHangUp();
	public pointcut ICustomerCall() : call(void telecom.v2.connect.ICustomer.call(..));
	public pointcut ICustomerPickUp() : call(void telecom.v2.connect.ICustomer.pickUp(..));
	public pointcut ICustomerHangUp() : call(void telecom.v2.connect.ICustomer.hangUp(..));
	public pointcut ICustomerFinal() : ICustomerCall() || ICustomerPickUp() || ICustomerHangUp();
	public pointcut ConnectionChangeState() : execution(telecom.v2.connect.Connection.new(..)) || execution(void telecom.v2.connect.Connection.complete()) || execution(void telecom.v2.connect.Connection.drop());
	
}
