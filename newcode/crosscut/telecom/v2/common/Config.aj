package telecom.v2.common;

import telecom.v2.billing.*;
import telecom.v2.time.*;
import telecom.v2.trace.*;

/**
 * Classe permettant de configurer les aspects
 * Elle défini, entre autre, l'ordre d'execution des aspects (leur précédence)
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 * 
 */
public aspect Config {

	declare precedence : BillManagement, TracingManagement, TimeManagement;
	
}
