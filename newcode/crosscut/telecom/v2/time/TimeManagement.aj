package telecom.v2.time;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import telecom.v2.common.Pointcuts;
import telecom.v2.util.Contract;
import telecom.v2.connect.ICustomer;
import telecom.v2.connect.Customer;
import telecom.v2.connect.Call;
import telecom.v2.connect.ICall;

/**
 * Classe qui gère la gestion du temps dans les autres classes
 * 
 * @author Chassat Romain, Etienne Tom, Hazard Alexandre
 */
public privileged aspect TimeManagement {
	
	
	/**
	 * 
	 * Gestion du temps pour la classe Connection
	 * 
	 */
	
	//ATTRIBUT
	
	private Timer telecom.v2.connect.Connection.calltimer;
	
	//REQUETES
	
	/**
	 * getter de calltimer dans la classe Connection
	 * @return Timer
	 */
	private Timer telecom.v2.connect.Connection.getTimer(){
		return this.calltimer;
	}
	
	/**
	 * setter de calltimer dans la classe Connection
	 * @param t
	 * @return nothing
	 */
	private void telecom.v2.connect.Connection.setTimer(Timer t){
		this.calltimer = t;
	}
	
	/**
	 * Méthode getDuration de la V1 :
	 *  Durée de la connexion.
     * @pre
     *     getState() == State.DROPPED
	 * @return Timer
	 */
	public int telecom.v2.connect.Connection.getDuration() {
		Contract.checkCondition(this.state == State.DROPPED);	
		return calltimer.getTime();
	}
	
	//ADVICES
	
	/**
	 * Initialise le timer dans Connection après l'éxecution de son Constructeur
	 * @param c l'instance de Connection
	 */
	after(telecom.v2.connect.Connection c) : target(c) && Pointcuts.ConnectionConstructorExec(){
		c.setTimer(new Timer());
	}
	
	/**
	 * Démarre le timer dans la classe Connection après l'appel à la méthode complete de Connection
	 * @param c l'instance de Connection
	 */
	after(telecom.v2.connect.Connection c) returning : target(c) && Pointcuts.ConnectionComplete() {
		c.getTimer().start();
	}
	
	/**
	 * Arrête le timer dans la classe Connection après l'appel à la méthode drop de Connection
	 * @param c l'instance de Connection
	 */
	after(telecom.v2.connect.Connection c) returning : target(c) && Pointcuts.ConnectionDrop() {
		c.getTimer().stop();
		int time = c.getTimer().getTime();
		c.getCaller().setTotalTime(c.getCaller().totalTime + time);
		c.getCallee().setTotalTime(c.getCallee().totalTime + time);
	}
	
	/**
	 * 
	 * Gestion du temps pour la classe Call et ICall
	 * 
	 */
	
	//ATTRIBUTS
	
	private Map<ICustomer, telecom.v2.connect.Connection> ICall.dropped;
	private Map<ICustomer, telecom.v2.connect.Connection> Call.dropped;
	
	//REQUETES
	
	/**
	 * setter de la Map dropped pour l'interface ICall
	 * @param m une Map
	 */
	private void ICall.setDropped(Map<ICustomer, telecom.v2.connect.Connection> m){
		this.dropped = m;
	}
	
	/**
	 * getter de la map dropped pour l'interface ICall
	 * @return Map<ICustomer, Connection>
	 */
	private Map<ICustomer, telecom.v2.connect.Connection> ICall.getDropped(){
		return this.dropped;
	}
	
	/**
	 * setter de la Map dropped pour la classe Call
	 * @param m une Map
	 */
	private void Call.setDropped(Map<ICustomer, telecom.v2.connect.Connection> m){
		this.dropped = m;
	}
	
	/**
	 * getter de la map dropped pour la class Call
	 * @return Map<ICustomer, Connection>
	 */
	private Map<ICustomer, telecom.v2.connect.Connection> Call.getDropped(){
		return this.dropped;
	}
	
	/**
	 * Déclaration de la méthode getElapsed dans l'interface ICall
	 * @param x ICustomer
	 */
	public abstract int ICall.getElapsedTimeFor(ICustomer x);
	
	/**
	 * Le temps de connexion de ce client.
     * Si celui-ci na pas été contacté pour cet appel, la valeur retournée est
     *  0.
     * Si celui-ci est l'appelant, la valeur retournée est la somme des durées
     *  de communication pour chacun des correspondant appelés qui ont répondu
     *  et déjà raccroché.
     * @pre
     *     x != null && !isConnectedWith(x)
	 */
	public int Call.getElapsedTimeFor(ICustomer x) {
        Contract.checkCondition(x != null);
        Contract.checkCondition(!isConnectedWith(x));

        if (x == caller) {
            int result = 0;
            for (telecom.v2.connect.Connection conn : dropped.values()) {
                result += conn.getDuration();
            }
            return result;
        } else {
        	telecom.v2.connect.Connection conn = dropped.get(x);
            if (conn == null) {
                return 0;
            } else {
                return conn.getDuration();
            }
        }
    }
	
	//ADVICES
	
	/**
	 * Utilise le setter de la map dropped dans la classe Call après l'éxecution du constructeur de Call
	 * @param c l'instance de Call
	 */
	after(Call c) : target(c) && Pointcuts.CallConstructorExec() {
		c.setDropped(new HashMap<ICustomer, telecom.v2.connect.Connection>());
	}
	
	/**
	 * Ajoute avant l'appel à la méthode hangup de Call,
	 * dans la map dropped de la classe Call, les connections qui vont être coupé
	 * @param c l'instance de Call
	 * @param cust le ICustomer passer en argument de la méhode hangup 
	 */
	before(Call c, ICustomer cust) : target(c) && args(cust) && Pointcuts.CallHangUp() {
		Contract.checkCondition(cust != null);
        Contract.checkCondition(cust == c.caller || c.isConnectedWith(cust));

        if (cust == c.caller) {
            for (ICustomer r : c.pending.keySet()) {
            	telecom.v2.connect.Connection conn = c.pending.get(r);
                c.getDropped().put(r, conn);
            }
            for (ICustomer r : c.complete.keySet()) {
            	telecom.v2.connect.Connection conn = c.complete.get(r);
                c.getDropped().put(r, conn);
            }
        } else {
        	telecom.v2.connect.Connection conn = c.complete.get(cust);
            c.getDropped().put(cust, conn);
        }
    }

	/**
	 * 
	 * Gestion du temps pour la classe Customer et ICustomer
	 * 
	 */
	
	//ATTRIBUT
	
	private int ICustomer.totalTime;
	
	//REQUETES
	
	/**
	 * setter de totalTime dans l'interface ICustomer 
	 * @param t le temps
	 */
	private void ICustomer.setTotalTime(int t){
		this.totalTime = t; 
	}
	
	/**
	 * getter de totalTime dans l'interface ICutomer
	 * @return totalTime int
	 */
	public int ICustomer.getTotalConnectedTime(){
		return this.totalTime;
	}
	
	/**
	 * setter de totalTime dans la classe Customer
	 * @param t le temps
	 */
	private void Customer.setTotalTime(int t){
		this.totalTime = t;
	}
	
	/**
	 * getter de totalTime dans la classe Cutomer
	 * @return totalTime int
	 */
	public int Customer.getTotalConnectedTime(){
		return this.totalTime;
	}
	
}
